
esc='\e'
export COLOR_RED="$esc[31m"
export COLOR_GRN="$esc[32m"
export COLOR_YLW="$esc[33m"
export COLOR_LRED="$esc[1;31m"
export COLOR_LGRN="$esc[1;32m"
export COLOR_LYLW="$esc[1;33m"


# becomes gray as foreground/ bright
black=30
red=31
green=32
yellow=33
blue=34
magenta=35
cyan=36
lgray=37
STYLE_GRAY="$esc[1;30m"

normal=$(tput sgr0)

bold="$esc[1m"
underline="$esc[4m"
highlight="$esc[7m"

function print-colored() {
	message="$1"
	# numerical value!
	color=$2
	appendix="$3"
	indent=${#appendix}
	# determine dynamically to capture resizing
	width=$(tput cols)
	cols=$((width - indent))
	# 1; makes it a bright/ foreground color, leave out to make it background/ dull
	printf '%s\033[%dG'"$esc"'[1;%dm%s'"$normal"'\n' "$message" "$cols" "$color" "$appendix"
}

function print-underlined() {
	message="$1"
	echo -e "\n${underline}${message}${normal}"
}

function print-inactive() {
	message="$1"
	echo -e "\n${STYLE_GRAY}${message}${normal}"
}


function print-ok() {
	message="$1"
	if [ $# -eq 2 ]; then
		appendix="$2"
	else
		appendix="[ OK ]"
	fi

	print-colored "$message" $green "$appendix"
}

function print-warn() {
	message="$1"
	if [ $# -eq 2 ]; then
		appendix="$2"
	else
		appendix="[WARN]"
	fi

	print-colored "$message" $yellow "$appendix"
}

function print-err() {
	message="$1"
	if [ $# -eq 2 ]; then
		appendix="$2"
	else
		appendix="[ERR ]"
	fi

	print-colored "$message" $red "$appendix"
}
