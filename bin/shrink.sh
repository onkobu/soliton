#!/bin/bash

# -E - inherit ERR trap
# -e - exit if compounds/ conditionals fail,  problematic with compound if
#      preferrably handle errors explicitely
# -u - fail on unset variables, forces correct expansion when testing for unset
# -o pipefail - fail pipes for any of the parts not only the last
#set -Eeuo pipefail
set -Euo pipefail

function usage {
cat <<EOF

    $(basename "${BASH_SOURCE[0]}") [-v]

Check a directory if it exceeds a size limit and if so try to free some space
by deleting files.

    -d <directory>  - directory to check, default .
    -l <limit>      - size limit in 1k blocks, 1000 is a MByte, default 100G
    -p <percentage> - allowed percentage of limit, default 90

If the directory consumes too much space deletion happens in two steps. First
all files older than 90 days are deleted. If this does not free sufficient
space all files older than 14 days are deleted. In the rare case this didn't
free enough space, too, the script exists with a non zero error code and
error message.
EOF
exit 0
}

# 100GB
limitSizeBlocks=100000000

thresholdPercent=90

baseDir=.

while getopts ":d:l:t:" o; do
    case "${o}" in
        d) baseDir=$OPTARG
           ;;
        l) limitSizeBlocks=$OPTARG
           ;;
        t) thresholdPercent=$OPTARG
           ;;
        *) usage
           ;;
    esac
done


currentSizeBlocks=$(du -ks $baseDir | cut -f1)

remaining=$(( limitSizeBlocks-currentSizeBlocks ))

remainingThreshold=$((limitSizeBlocks*(100-thresholdPercent)/100))

targetSize=$(( limitSizeBlocks*thresholdPercent/100 ))

function more-shrink-needed {
        local currSize=$(du -ks $baseDir | cut -f1)
        local targSize=$(( limitSizeBlocks*thresholdPercent/100 ))
        [[ $currSize -gt $targSize ]] && return

        false
}

function remove-older-than-days {
        days="$1"

        echo "Removing all files older than $days days"

        # TODO: add -delete
        find $baseDir -mtime "+${days}" -type f >/dev/null 2>&1
}

function error-out {
        message="$1"
        errCode="$2"

        >&2 echo "$message"
        exit $errCode
}

function shrink-limit-exceeded {
        # Maybe there are more agressive options needed. Until now
        # shrink towards healty limit
        shrink-within-limit
}

function shrink-within-limit {
        remove-older-than-days 90
        if more-shrink-needed; then
                remove-older-than-days 14
                if more-shrink-needed; then
                        error-out "Could not remove sufficient amount of files" 7
                fi
        fi
}

if [ $remaining -lt 0 ]; then
        echo "Limit of $limitSizeBlocks blocks exceeded by $remaining blocks"
        shrink-limit-exceeded
elif [ $remaining -lt $remainingThreshold ]; then
        echo "Total size of $currentSizeBlocks blocks and remaining size of $remaining is"
        echo "less than allowed ${thresholdPercent}% fill rate of $limitSizeBlocks blocks"
        shrink-within-limit
else
        echo "Everything fine, size of $currentSizeBlocks blocks within ${thresholdPercent}%"
        echo "of $limitSizeBlocks blocks"
fi
