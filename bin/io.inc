

check-mounted() {
    path=$1

    if mountpoint $path >/dev/null 2>&1; then
		print-warn $path "mounted"
    else
		print-ok $path "not mounted"
    fi
}

