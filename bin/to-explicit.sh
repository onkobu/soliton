#!/bin/bash

function print_usage_and_exit {
	cat <<EOF

	to-explicit.sh <jar-file>

Turns an automatic module - one without a module-info.java - into a valid
named module. Some libraries are still not migrated in 2021. The tedious
sequence of steps was merged into this script.

EOF
	exit 0

}

if [ "$1" = "-h" ] || [ "$1" = '--help' ] || [ $# -ne 1 ]; then
	print_usage_and_exit
fi

toolsFailed=0

function check_command {
	cmd=$1
	hash $cmd
	if [ $? -ne 0 ]; then
		echo "Command $cmd not found"
		toolsFailed=1
	fi
}

check_command jdeps
check_command javac
check_command jar

if [ $toolsFailed -ne 0 ]; then
	echo "Cannot continue without the tool(s)"
	exit 17
fi

jarName=$1

if [ ! -f $jarName ]; then
	echo "JAR $jarName does not exist"
	exit 7
fi

echo "generating module info"
moduleInfoFile=$(jdeps --generate-module-info . $jarName | awk '{print $3}')

echo module info $moduleInfoFile

moduleName=$(echo $moduleInfoFile | awk -F'/' '{print $2}')

echo "javac --patch-module module $moduleName"
javac --patch-module $moduleName=$jarName $moduleInfoFile

echo "updating JAR with module-info.class"
jar uf $jarName -C $moduleName module-info.class

if [ -f ${jarName}.sha1 ]; then
	echo "Re-writing SHA1 file"

	sha1sum=$(sha1sum -b $jarName | awk '{print $1}')
	echo -n "$sha1sum" >${jarName}.sha1
fi
