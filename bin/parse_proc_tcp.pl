#!/usr/bin/perl

my @TCP_STATES = (
	"TCP_ESTABLISHED" ,
	"TCP_SYN_SENT",
	"TCP_SYN_RECV",
	"TCP_FIN_WAIT1",
	"TCP_FIN_WAIT2",
	"TCP_TIME_WAIT",
	"TCP_CLOSE",
	"TCP_CLOSE_WAIT",
	"TCP_LAST_ACK",
	"TCP_LISTEN",
	"TCP_CLOSING",
	"TCP_NEW_SYN_RECV",
	"TCP_MAX_STATES"
);

sub parseIpAndPort {
	my ($source)=@_;
	
	my ($sIp, $sPort) = split(":", $source);
	my @ip = map hex($_), ( $sIp =~ m/../g );
	$ipStr = join('.',reverse(@ip));
	my $port = hex($sPort);
	return "$ipStr:$port";
}

foreach my $line ( <STDIN> ) {
    chomp( $line );
	my ($id, $source, $dest, $state) = split(" ", $line);
	
	print $id, parseIpAndPort($source), " -> ", parseIpAndPort($dest), " ", $TCP_STATES[$state], "\n";
}