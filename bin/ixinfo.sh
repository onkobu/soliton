#!/bin/bash

source `dirname "$0"`/output.inc

print-underlined "\nKernel"
uname -a

print-underlined "\nDistribution"
cat /etc/*-release

echo ""
command -v lsb_release >/dev/null 2>&1
if [ $? -eq 0 ]; then
	lsb_release
else
	print-inactive "<lsb_release not available>"
fi

echo ""
command -v hostnamectl >/dev/null 2>&1
if [ $? -eq 0 ]; then
	hostnamectl
else
	print-inactive "<no systemd environment/ no hostnamectl>"
fi
