#!/bin/bash

case $1 in
-l)echo $(date +%Y_%m_%d-%H_%M_%S);;
-s)echo $(date +%Y_%m_%d);;
-h)echo "create log file name, use -l to create long format";;
*)echo $(date +%Y_%m_%d);;
esac
