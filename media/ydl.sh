#!/bin/bash

if [ $# -eq 0  -o "$1" = "-h" -o "$1" = "--help" ]; then
	echo "Download youtube videos and extract MP3s"
	echo ""
	echo "  ydl <file>"
	echo ""
	echo "<file> - a list of <url>;<artist>;<title>;<album>;<genre-id>"
	echo ""
	echo "Adds rate limiting with random part, encoding as MP3 and"
	echo "adding ID3v2-tags"
	echo "This is a soliton script: https://codeberg.org/onkobu/soliton"
	echo ""
	exit 0
fi

if [ ! -f youtube-dl ]; then
	echo "No youtube-dl in current directory"
	exit 7
fi

echo "Updating first"
./youtube-dl -U

while read i; do
	IFS=';' read -r -a array <<< "$i"
	url="${array[0]}"
	artist="${array[1]}"
	title="${array[2]}"
	album="${array[3]}"
	genre="${array[4]}"
	videoTitle=$(./youtube-dl --get-title "$url")
	rateLimit=$(shuf -i 50-130 -n 1)
	echo "Download $videoTitle ($url)"
	./youtube-dl --extract-audio --audio-format mp3 --embed-thumbnail --audio-quality 2 -r ${rateLimit}K -o '%(title)s.%(ext)s' "$url"
	errCode=$?

	if [ $errCode -ne 0 ]; then
		echo "No tagging without successful download"
		exit $errCode
	fi

	echo "Tag $artist - $title, $album, $genre"
	if [ -z "$album" ]; then
		id3v2 -a "$artist" -t "$title" -g $genre -c "$url" "${videoTitle}.mp3"
		errCode=$?
	else
		id3v2 -a "$artist" -t "$title" -A "$album" -g $genre -c "$url" "${videoTitle}.mp3"
		errCode=$?
	fi

	if [ $errCode -ne 0 ]; then
		echo "Could not tag successfully, will not continue"
		exit $errCode
	fi
done < <(awk '!/^ *#/ && NF' $1)
