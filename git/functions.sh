function str_elipsis {
	str=$1
	len=$2

	if [ ${#str} -gt $len ]; then
		shortened=${str:0:$((len-3))}
		echo "${shortened}..."
	else
		echo $str
	fi
}

# Checks whether the current directory contains changes
#
function git_is_modified {
	if [[ -z $(git diff --stat) ]]; then
		echo 0
	else
		echo 1
	fi
}

# Checks whether the current directory has untracked files
# (Git)
function git_has_untracked {
	modified=$1

	if [ $modified -eq 0 -a -n "$(git status -s | grep '??')" ]; then
		echo 1
	else
		echo 0
	fi
}

# Checks for commits not pushed yet
# (Git)
function git_has_commits2push {
	if [ -n "$(git status | grep '"git push"')" ]; then
		echo 1
	else
		echo 0
	fi
}
