#!/bin/bash

# -E - inherit ERR trap
# -e - exit if compounds/ conditionals fail,  problematic with compound if
#      preferrably handle errors explicitely
# -u - fail on unset variables, forces correct expansion when testing for unset
# -o pipefail - fail pipes for any of the parts not only the last
#set -Eeuo pipefail
set -Euo pipefail

function usage {
cat <<EOF

    $(basename "${BASH_SOURCE[0]}") <jira-ticket>

Finds the branch name containing the JIRA-ticket number.

    jira-ticket - partial JIRA ticket number

EOF
exit 0
}

opt=${1+$1}

if [ $# -eq 0 ] || [ $# -gt 1 ] || [ ${opt+$opt}  = '-h' ] || [ ${opt+$opt} = '--help' ]; then
        usage
        exit 0
fi

branch=$(git branch | grep "$opt")
if [ -z "$branch" ]; then
        remoteBranch=$(git branch -a | grep "$opt")

        if [ -z "$remoteBranch" ]; then
                >&2 echo "No branch containing $opt found:"
                git branch -a
                exit 3
        else
                branch="${remoteBranch:17}"
        fi
fi

echo "$branch" | xargs git checkout
