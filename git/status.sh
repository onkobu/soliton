#!/bin/bash

if [ $# -ne 0 -o "$1" = '-h' -o "$1" = '--help' ]; then
    echo ""
    echo "$(basename $0) - Git repo status to STDOUT"
    echo ""
    echo "Tries all subdirectories of the current directory and displays current"
    echo "branch checked out as well as status column. Status can be OK with no"
    echo "changes, UNTRAC with untracked files only or DRTY with pending changes."
	echo "This is a soliton script: https://codeberg.org/onkobu/soliton"
    echo ""
    exit 0
fi

. `dirname "$0"`/../bin/output.inc
. `dirname "$0"`/functions.sh

projects="$(find . -maxdepth 1 ! -path . -type d)"

PATTERN='%-25s %-45s %s'
for project in $projects; do
	directory=$(basename $project)
	cd $directory

	git status >/dev/null 2>&1
	if [ $? -ne 0 ]; then
		printf -v msg "$PATTERN" $directory "---" "$STYLE_GRAY[NONGIT]$normal"
		echo -e "$msg"
		cd ..
		continue
	fi

	modified=$(git_is_modified)

	untracked=$(git_has_untracked $modified)

	topush=$(git_has_commits2push)

	#echo $modified $untracked
	skip=0
	if [ $modified -eq 1 ]; then
		status="$COLOR_LRED[ DRTY ]$normal"
		skip=1
	elif [ $untracked -eq 1 ]; then
		status="$COLOR_LYLW[UNTRAC]$normal"
	elif [ $topush -eq 1 ]; then
		status="$COLOR_LYLW[ PUSH ]$normal"
	else
		status="$COLOR_LGRN[  OK  ]$normal"
	fi
	branch=$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')

	dirText=$(str_elipsis $directory 25)

	# with branches labelled meaningful there'll be plenty of space
        # necessary for JIRA task number and some text
        #
	printf -v msg "$PATTERN" $dirText $branch "$status"
	echo -e "$msg"
	
	cd ..
done
