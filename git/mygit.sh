#!/bin/bash

# -E - inherit ERR trap
# -e - exit if compounds/ conditionals fail,  problematic with compound if
#      preferrably handle errors explicitely
# -u - fail on unset variables, forces correct expansion when testing for unset
# -o pipefail - fail pipes for any of the parts not only the last
#set -Eeuo pipefail
set -Euo pipefail

esc='\e'
red=31
#green=32
yellow=33
normal=$(tput sgr0)

function usage {
cat <<EOF

    $(basename "${BASH_SOURCE[0]}") <command> [command-args]

Wraps complex git actions in meta commands. The following commands are available:

    broom          - show stale branches and optionally clean them
	into-main	   - merge the current branch into main and clean up branch
    fetch-pull     - fetch and pull current branch
    premerge       - switch to master, fetch & pull, return to selected
    prepare-squash - soft reset current non-master branch for new commit of
                     everything changed with this branch
    rename         - rename the current branch, new name is the only parameter
EOF
}

parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'
}

exit_on_error() {
        errCode="$1"
		message="${2+$2}"

		if [ -n "$message" ]; then
			>&2 print_colored "$message" "$red" "[ERROR]"
		fi

        if [ "$errCode" -ne 0 ]; then
                exit "$errCode"
        fi
}

function print_colored {
	message="$1"
	color="$2"
	appendix="$3"

	indent=${#appendix}
	width=$(tput cols)
	cols=$((width - indent))
	# 1; makes it a bright/ foreground color, leave out to make it background/ dull
	printf '%s\033[%dG'$esc'[1;%dm%s'"$normal"'\n' "$message" "$cols" "$color" "$appendix"
}

function show_warning {
	# determine dynamically to capture resizing
	print_colored "$1" "$yellow" "[ WARN ]"
}

function show_error {
	# determine dynamically to capture resizing
	print_colored "$1" "$red" "[  ERR ]"
}


function premerge {
        if [ -z "$masterName" ]; then
                >&2 echo "Master branch of the current repository $repo is not configured, assuming master"
                operatingMaster="master"
        else
                operatingMaster="$masterName"
        fi

        echo "Working in repo $repo, master name $masterName"

        switchBack=0

        if [ ! "$currentBranch" = "$operatingMaster" ]; then
                switchBack=1
                git checkout "$operatingMaster"
                exit_on_error $?
        fi

        fetch-pull

        if [ "$switchBack" -ne 0 ]; then
                git checkout -
                exit_on_error "$?"
        fi

}

function rename {
        newName="$1"
        read -p "Are you sure to rename $currentBranch to $newName? [y/N] " -n 1 -r

        echo    # new line
        if [[ $REPLY =~ ^[Yy]$ ]]; then
                git branch -m "$newName"
                exit_on_error "$?"
                git push origin -u "$newName"
                exit_on_error "$?"
                git push origin --delete "$currentBranch"
        fi
}

function prepare-squash {
        if [ "$currentBranch" = "$masterName" ]; then
                show_warning "Cannot prepare squash on master branch"
                return
        fi

        read -p "Are you sure to soft reset $currentBranch to issue a new commit? [y/N] " -n 1 -r

        echo    # new line
        if [[ $REPLY =~ ^[Yy]$ ]]; then
                git reset --soft "$(git merge-base main HEAD)"
                exit_on_error $?
                read -p "Immediately issue git commit? [y/N] " -n 1 -r

                echo    # new line
                if [[ $REPLY =~ ^[YyJj]$ ]]; then
                        git commit
                fi
        fi

}

function into-main {
        if [ "$currentBranch" = "$masterName" ]; then
                show_warning "On master branch. Checkout the branch to be merged instead."
                return
        fi

		# just for readability
		branch="$currentBranch"

		if ! git checkout "$masterName"; then
			show_error "Failed to checkout $masterName"
			return
		fi
		if ! git merge "$branch"; then
			show_error "Failed to merge $branch into $masterName"
			return
		fi

        read -p "Push current master? [Y/n] " -n 1 -r

        echo    # new line
        if [[ -z "$REPLY" ]] || [[ "$REPLY" =~ ^[YyJj]$ ]]; then
			if ! git push; then
				>&2 echo "Failed to push $masterName"
				return
			fi
		fi

		read -p "Remove branch ${branch} (local and remote)? [Y/n] " -n 1 -r

        echo    # new line
        if [[ -z "$REPLY" ]] || [[ "$REPLY" =~ ^[YyJj]$ ]]; then
			if ! git branch -d "$branch"; then
				>&2 echo "Failed to delete $branch locally"
				return
			fi
			if ! git push origin --delete "$branch"; then
				>&2 echo "Failed to push-delete $branch remotely"
				return
			fi
		fi
}

function fetch-pull {
		currentBranch=$(parse_git_branch)
		if [ "$currentBranch" != "$masterName" ]; then
			show_warning "Pulling non-master branch"
		fi

        git fetch --all --prune && git pull
        exit_on_error $?
}

function broom {
        toBroom=$(git branch -vv | grep ': gone]' | grep -v "\*" | awk '{print $1;}')

		if [ -z "$toBroom" ]; then
			echo "All clean"
			return
		else
			echo "$toBroom"
		fi

        read -p "Remove those branches locally? [y/N] " -n 1 -r

        echo    # new line
        if [[ $REPLY =~ ^[Yy]$ ]]; then
                git branch -vv | grep ': gone]' | grep -v "\*" | awk '{print $1;}' | xargs -r git branch -D
        fi
}

cmd=${1+$1}

if [ -z "$cmd" ] || [ "$cmd" = '-h' ] || [ "$cmd" = '--help' ]; then
        usage && exit 0
fi

if ! hash git >/dev/null 2>&1; then
        show_error "Git-executable not available."
        exit 7
fi

repo=$(basename "$PWD")
currentBranch=$(parse_git_branch)

if [ -z "$currentBranch" ]; then
        show_error "Current directory does not yield a Git branch."
        exit 3
fi

masterName=$(git symbolic-ref refs/remotes/origin/HEAD | sed 's@^refs/remotes/origin/@@')

case "$cmd" in
        broom) broom
                ;;
        fetch-pull) fetch-pull
			    ;;
		into-main) into-main
                ;;
        premerge) premerge
                ;;
        prepare-squash) prepare-squash
                ;;
        rename) rename "$2"
                ;;
        *) usage
                ;;
esac
