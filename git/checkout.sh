#!/bin/bash

if [ $# -ne 1 -o "$1" = '-h' -o "$1" = '--help' ]; then
    echo ""
    echo "$(basename $0) - Checkout Git branch"
    echo ""
    echo "Tries all subdirectories of the current directory and checks out a given"
    echo "branch. Errors will be written to STDERR, status to STDOOUT. If the branch"
    echo "does not exist or cannot be checked out otherwise, e.g. because of pending"
    echo "changes, the script will continue."
	echo "This is a soliton script: https://codeberg.org/onkobu/soliton"
    echo ""
    exit 0
fi

targetBranch=$1

. `dirname "$0"`/../bin/output.inc
. `dirname "$0"`/functions.sh

projects="$(find . -maxdepth 1 ! -path . -type d)"

PATTERN='%-25s %-45s %s'

for project in $projects; do
	directory=$(basename $project)
	cd $directory
	
	git status >/dev/null 2>&1
	if [ $? -ne 0 ]; then
		printf -v msg "$PATTERN" $directory "---" "$COLOR_GRAY[NONGIT]$normal"
		echo -e "$msg"
		cd ..
		continue
	fi


	if [ "$targetBranch" = '' ]; then
		repoTargetBranch=$(basename `cat .git/refs/remotes/origin/HEAD | awk '{ print $2 }'`)
	else
		repoTargetBranch="$targetBranch"
	fi

	modified=$(git_is_modified)

	untracked=$(git_has_untracked $modified)

	#echo $modified $untracked
	skip=0
	if [ $modified -eq 1 ]; then
		status="$COLOR_LRED[ DRTY ]$normal"
		skip=1
	elif [ $untracked -eq 1 ]; then
		status="$COLOR_LYLW[UNTRAC]$normal"
	else
		status="$COLOR_LGRN[  OK  ]$normal"
	fi
	branch=$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/')

	if [ "$repoTargetBranch" = "$branch" ]; then
		status="$COLOR_GRN[ KEEP ]$normal"
		skip=1
	fi
	dirText=$(str_elipsis $directory 25)

	errCode=0
	if [ $skip -eq 0 ];then
		git checkout $repoTargetBranch
		errCode=$?
	fi

	if [ $errCode -ne 0 ]; then
		status="$COLOR_LRED[ ERR  ]$normal"
	fi

	# with branches labelled meaningful there'll be plenty of space
        # necessary for JIRA task number and some text
        #
	printf -v msg "$PATTERN" $dirText $branch "$status"
	echo -e "$msg"
	
	cd ..
done
