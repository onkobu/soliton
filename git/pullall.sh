#!/bin/bash

if [ $# -ne 0 -o "$1" = '-h' -o "$1" = '--help' ]; then
    echo ""
    echo "$(basename $0) - pull all repos in current directory"
    echo ""
    echo "Tries all subdirectories of the current directory as Git repositories"
    echo "and pulls latest version incl. git fetch --all. Pull is always invoked "
    echo "for the current branch. If the Git repo containes uncommitted changes"
    echo "nothing is pulled. In contrast untracked files are no problem."
	echo "This is a soliton script: https://codeberg.org/onkobu/soliton"
    echo ""
    exit 0
fi

. `dirname "$0"`/../bin/output.inc
. `dirname "$0"`/functions.sh

projects="$(find . -maxdepth 1 ! -path . -type d)"

PATTERN='%-25s %45s %s'
for project in $projects; do
	directory=$(basename $project)
	cd $directory

	echo "$directory"
	
	git status >/dev/null 2>&1
	if [ $? -ne 0 ]; then
		printf -v msg "$PATTERN" $directory "---" "$COLOR_GRAY[NONGIT]$normal"
		echo -e "$msg"
		cd ..
		continue
	fi

	
	modified=$(git_is_modified)

	untracked=$(git_has_untracked $modified)

	#echo $modified $untracked
	skip=0
	if [ $modified -eq 1 ]; then
		status="$COLOR_LRED[ DRTY ]$normal"
		skip=1
	elif [ $untracked -eq 1 ]; then
		status="$COLOR_LYLW[UNTRAC]$normal"
	else
		status="$COLOR_LGRN[  OK  ]$normal"
	fi

	if [ $skip -eq 0 ]; then
		git fetch --all --prune
		git pull
		repoMsg="fetch all incl. prune and pull"
	else
		repoMsg="skipping pull"
	fi

	printf -v msg "$PATTERN" $directory "$repoMsg" "$status"
	echo -e "$msg"
	
	cd ..
done
