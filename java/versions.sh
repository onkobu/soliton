#!/bin/bash

if [ $# -ne 0 -o "$1" = '-h' -o "$1" = '--help' ]; then
    echo ""
    echo "$(basename $0) - Maven versions to STDOUT"
    echo ""
    echo "Tries all subdirectories of the current directory containing a pom.xml."
    echo "For all being found <version> is printed to STDOUT. Comes in handy with"
    echo "many repositories, branches and the urge to find the version for an"
    echo "upcoming release."
	echo "This is a soliton script: https://codeberg.org/onkobu/soliton"
    echo ""
    exit 0
fi


poms=$(find ./ -maxdepth 2 -name pom.xml)

for pomFile in $poms; do
	repo="$(dirname $pomFile)"
	cd "$repo"
	version=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout)
	printf -v msg '%-40s %s' $repo $version
	echo "$msg"
	cd - > /dev/null 2>&1
done
