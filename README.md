# soliton

A soliton wave does not change shape while travelling through a medium. This repository washes scripts onto your linux machine not changing their shape. Such soliton scripts transport the energy from your fingers to any state machine they're made for.

# Git

Run these from a parent directory, e.g. $HOME/git, to check all sub directories.

* status.sh – check for untracked files, hanging commits and so on
* fetchall.sh – fetch all updates
* pullall.sh – pull (for current branch)
* mygit.sh - complex operations like merging branches, smash commits and more

# Java

* bin/jdks – selects a JDK for the current shell based on your list of installed JDKs
* bin/mvns – selects a Maven variant based on your list of installed versions
* bin/to-exact.sh – Turns an automatic module JAR into an exact module = makes it `jlink`-able
* java/version.sh – extract version from pom.xml

# Media

* media/ydl.sh – wrap youtube-dl to downloads lists of videos as MP3s with ID3 tagging
